package com.example.imageviewer_ug;

import android.Manifest;
import android.content.ClipData;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import android.provider.MediaStore;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            final int REQUEST_IMAGE_CAPTURE = 101;
            //instance variables for the floatingActionButtons
            Button Gallery;
            final FloatingActionButton btn = findViewById(R.id.btn_float);
            FloatingActionButton btn_camera = findViewById(R.id.btn_camera);

            //Button click listener to take camera picture
            btn_camera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    {
                        Snackbar.make(view, "Camera Opened", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                        Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (camera.resolveActivity(getPackageManager()) != null) {

                            startActivityForResult(camera, REQUEST_IMAGE_CAPTURE);

                        }


                    }
                }
            });

            //Listening method for checking storage permissions
            Gallery = findViewById(R.id.Gallery);
            Gallery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, "Gallery opened", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(MainActivity.this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                100);
                        return;
                    }


                    //Implicit intent for selecting multiple images from the gallery
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    intent.setType("image/*");
                    startActivityForResult(intent, 1);

                    //Intent for share feature to any social media once installed on user phone
                    btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Snackbar.make(view, "Share options", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                            Intent intent = new Intent(Intent.ACTION_SEND);
                            intent.setType("text/plain");
                            String shareBody = "Your body here";
                            String shareSub = "Your Subject here";
                            intent.putExtra(Intent.EXTRA_SUBJECT, shareBody);
                            intent.putExtra(Intent.EXTRA_TEXT, shareSub);
                            startActivity(Intent.createChooser(intent,"Share using"));


                        }
                    });

                }
            });

        }


        @Override
        protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
//Bitmaps for converting images
            final int REQUEST_IMAGE_CAPTURE = 101;
            ImageView imageView = findViewById(R.id.viewImages);

            if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {

                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                imageView.setImageBitmap(imageBitmap);


            }



//Bitmaps to determine if any keeps were selected and added to and arraylist using bitmaps
            if (requestCode == 1 && resultCode == RESULT_OK) {
                final ImageView viewImages = findViewById(R.id.viewImages);
                final List<Bitmap> bitmaps = new ArrayList<>();
                ClipData clipData =  data.getClipData();
                if (clipData != null) {
                    for (int i = 0; i < clipData.getItemCount(); i++) {

                        Uri imageUri = clipData.getItemAt(i).getUri();

                        try {
                            InputStream is = getContentResolver().openInputStream(imageUri);
                            Bitmap bitmap = BitmapFactory.decodeStream(is);
                            bitmaps.add(bitmap);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                } else {

                    Uri imageUri = data.getData();
                    try {
                        //assert imageUri != null;
                        InputStream is = getContentResolver().openInputStream(imageUri);
                        Bitmap bitmap = BitmapFactory.decodeStream(is);
                        bitmaps.add(bitmap);

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                }
                //Runnable thread for showing images as a slideshow
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        for (final Bitmap b : bitmaps) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    viewImages.setImageBitmap(b);
                                }
                            });

                            try {
                                Thread.sleep(3000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }).start();

            }

        }
    }
